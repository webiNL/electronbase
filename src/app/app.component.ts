import { Component } from '@angular/core';
import { ElectronService } from './providers/electron.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor (
    public electron: ElectronService,
    private translate: TranslateService
  ) {

    this.translate.setDefaultLang('en');

    if (this.electron.isElectron()) {
      // console.log('Mode electron');
      // console.log('Electron ipcRenderer', electron.ipcRenderer);
      // console.log('NodeJS childProcess', electron.childProcess);
    } else {
      // console.log('Mode web');
    }
  }
}
