export const AppConfig = {
  production: false,
  environment: 'LOCAL',
  server: 'http://localhost',
  port: 4300,
};
