export const AppConfig = {
  production: true,
  environment: 'PROD',
  server: 'https://getbarista.app',
  port: 80,
};
