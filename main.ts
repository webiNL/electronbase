import { app, BrowserWindow, screen } from 'electron';
import { join } from 'path';
import { format } from 'url';
import { platform } from 'os';

const args = process.argv.slice(1);
const serve = args.some(val => val === '--serve');
const screenSize = { width: 800, height: 500 };

let win: BrowserWindow;

try {
  app.on('ready', () => {
  createWindow();
});

  app.on('window-all-closed', () => {
    if (!isMac()) { app.quit(); }
  });

  app.on('activate', () => {
    if (win === null) { createWindow(); }
  });
} catch (e) { console.error(e.text); }


function isWin32() {
  return platform() === 'win32';
}

function isMac() {
  return platform() === 'darwin';
}

function startElectronReload() {
  require('electron-reload')(__dirname, {
    electron: require(`${__dirname}/node_modules/electron`)
  });
}

function createWindow() {
  const electronScreen = screen;
  const size = electronScreen.getPrimaryDisplay().workAreaSize;

  // Create the browser window.
  win = new BrowserWindow({
    ...screenSize,
    // Other options go here
  });

  if (serve) {
    startElectronReload();
    win.loadURL('http://localhost:4200');
    win.webContents.openDevTools();
  } else {
    win.loadURL(format({
      pathname: join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
  }

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store window
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });

}
