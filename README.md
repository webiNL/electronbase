# Webi Angular Sessie Part 2
## Electron Boogaloo

### Installing

- Git Pull this file
- Run `npm i` in the folder

### Starting for Development

- Run `npm start`
- Wait for the magical screen to turn on

### Running an install for other people

- Run `npm run build:prod`
- That's it, really...

### Any Questions
Ask for Kevin.